import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  welcomeMessage = 'You are logged in as';
  user = '';
  loggedIn = false;

  onLoginClick() {
    this.logInAs('Temporary User');
  }

  onLogoutClick() {
    this.logOut();
  }

  validateToken(token, key) {

  }

  token() {
    if (location.hash === '#nano') {
      this.logInAs('Respected NanoAuth User');
    } else {
      this.logOut();
    }
  }

  logInAs(name: string) {
    this.user = name;
    this.loggedIn = true;
  }

  logOut() {
    this.user = '';
    this.loggedIn = false;
    location.hash = '';
  }

}

// validJWT = {
//   // goes after # in browser
//   jwt: 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJkb2RpZCI6IlNUQVJLLlRPTlkuRS4xMjM0NTY3ODkwIiwiZmlyc3ROYW1lIjoiVG9ueSIsImxhc3ROYW1lIjoiU3RhcmsiLCJtaWRkbGVJbml0aWFsIjoiRSIsInVzZXJfbmFtZSI6IlNUQVJLLlRPTlkuRS4xMjM0NTY3ODkwIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sImF1dGhvcml0aWVzIjpbIlJPTEVfVVNFUiJdLCJqdGkiOiJjZGIwMTNmNi05MjEzLTQxZDgtOGQ1ZS1kYWY1NTkzMDNhY2MiLCJlbWFpbCI6InRvbnlzQHN0YXJraW5kdXN0cmllcy5jb20iLCJjbGllbnRfaWQiOiJzYW1wbGVDbGllbnRJZCJ9.F8kK4YLHNt-pI-eO1wT8ejeaEtyUhkwsYMMu7uLyFDTqewrqVz6y2nBc6XhcVe9n6bgxa_0TrVfFPTFRrjsV1jdOds1mc09V3DfGKNPZjhXLEIRJx6u0tFbQ0RqkKt41xqhpvmzOp10S733IflQyCVBI-VM1QeuXsV7o6EIL58RyFbtMm7UjT00OuGFa4YM3FctKyK-QwaiWGQX6LTrsRLFOmtBBe1YOhcugIuxaQGFdG9zn5dywsMgZ5Nwhm0F4Flkcu75rXYvhU5sFmRaI2Z1JHpoF5Nx7qhXMkGuLzBQZS_fac2w2KvszRD_SBe0loE5lvbVJBQb85IQub1AWGQ'
// }